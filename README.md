# Luciérnaga - Medalla PCB v1.0

<div align="center">
<img src="img/firefly_1.png/"
     alt="render_iso"
     height="400">
</div>

Corresponde a una placa de circuito electrónica (PCB) desarrollada como material didáctico para el taller de prototipado electrónico impartido en el FabLab U. de Chile. 

El propósito de este recurso es que, los participantes del taller, puedan aprender a emplear la máquina CNC Modela-MDX20 disponible en el FabLab para poder fabricar PCBs.
## Componentes
 
La PCB contempla:

- 1 x LED Amarillo
- 1 x resistencia de 200 ohm

## Diseño

El prototipo actual se ha diseñado en una protoboard, y su diagrama de conexiones se ha definido como sigue:

[foto]

Próximos pasos:
- Ensayos y calibración de sensor
- Diseño de essquemático y placa de circuitos PCB


## Testeo

Codigo de prueba:

## Programación

## Archivos


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fablab-u-de-chile/temporizador-pomodoro.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment

Integrantes:

